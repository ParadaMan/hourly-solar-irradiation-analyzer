# Hourly Solar Irradiation Analyzer

Hourly Solar Irradiation Analyzer (HSIA) is a software for personal use developped in Python 3 by Lu�s Parada. Its purpose is to read solar irradiation 
.csv datasets available from http://www.soda-pro.com/web-services/radiation/helioclim-3-archives-for-free and display graphically the 
irradiation profile of a given day, between 01-02-2004 and 31-12-2006, selected by the user.
 
 
## Requirements <a name="requirements"></a>

- **pandas**
- **matplotlib.pyplot**
- **numpy**
- **scipy.interpolate**


## Files <a name="files"></a>

- **irradiation_analyzer.py** - Source code file that shall be run to begin software execution. 

- **SoDa_HC3-METEO_pampilhosa_da_serra.csv** - Dataset file example.

In order to run the program the user must change the database path set in **irradiation_analyzer.py**


### Global Methods <a name="global_methods"></a>

- **read_df**(`string` file_path, `int` year, `int` month, `int` day) - Reads and stores radiation data for a selected date.

- **time_per_extent**(`float` hour_float) - converts float value of time in hours to string distinguishing hours, minutes and seconds. 

- **analyze_df**(`dict`) - interpolates data bewteen known data points and computes statistical information.  

- **plot_data**('numpy.ndarray' time, 'numpy.ndarray' irradiance, `int` year, `int` month, `int` day) - Plots hourly solar irradiation distribution throughout the selected date.





