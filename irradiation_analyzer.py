import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import pchip


def read_df(file_path, year, month, day):
    # returns irradiance data of a specified day

    df = pd.read_csv(file_path)
    time = []
    irradiance = []
    lower_bound = []
    upper_bound = []
    top_of_atm = []
    clear_sky = []
    irradiation = []

    for i in range(len(df)):
        if df.iloc[i, 0] == str(year) and df.iloc[i, 1] == str(month) and df.iloc[i, 2] == str(day):

            try:
                df.iloc[i, 3] = float(df.iloc[i, 3])
                df.iloc[i, 4] = float(df.iloc[i, 4])
                df.iloc[i, 5] = float(df.iloc[i, 5])
                df.iloc[i, 6] = float(df.iloc[i, 6])
                df.iloc[i, 7] = float(df.iloc[i, 7])
                df.iloc[i, 8] = float(df.iloc[i, 8])
                df.iloc[i, 9] = float(df.iloc[i, 9])

            except ValueError:
                # if cell contains value not convertible to float entire row is ignored
                print('Data file Reading Warning: row number %.0f contains data value non convertible to float, '
                      'row %.0f ignored\n' % (i, i))
                continue

            time.append(float(df.iloc[i, 3]))
            irradiance.append(float(df.iloc[i, 4]))
            lower_bound.append(float(df.iloc[i, 5]))
            upper_bound.append(float(df.iloc[i, 6]))
            top_of_atm.append(float(df.iloc[i, 7]))
            clear_sky.append(float(df.iloc[i, 8]))
            irradiation.append(float(df.iloc[i, 9]))

    return {'time': time, 'irradiance': irradiance, 'lower_bound': lower_bound, 'upper_bound': upper_bound,
            'top_of_atm': top_of_atm, 'clear_sky': clear_sky, 'irradiation': irradiation}


def time_per_extent(hour_float):

    hour = int(hour_float)
    minute = int((hour_float - hour) * 60)
    second = int(((hour_float - hour) * 60 - minute) * 60)

    time_string = []
    time_scale_string = ['hour', 'minute', 'second']
    i = 0
    for time_scale in [hour, minute, second]:
        if 1 < time_scale < 2:
            time_string.append(str(time_scale) + ' ' + time_scale_string[i])
        if time_scale > 2:
            time_string.append(str(time_scale) + ' ' + time_scale_string[i] + 's')
        else:
            i = i + 1
            continue
        i = i + 1

    if len(time_string) == 1:
        time_string_result = time_string
    if len(time_string) == 2:
        time_string_result = time_string[0] + ' and ' + time_string[1]
    if len(time_string) == 3:
        time_string_result = time_string[0] + ', ' + time_string[1] + ' and ' + time_string[2]

    return time_string_result


def analyze_df(data):
    try:
        time_new = np.linspace(data['time'][0], data['time'][-1], 500)
    except IndexError:
        print('Input date not available in database. Exiting program.')
        return

    irradiance_smooth = pchip(data['time'], data['irradiance'])

    for i in range(len(time_new)):
        if irradiance_smooth(time_new)[i] > 0.0 >= irradiance_smooth(time_new)[i - 1]:
            sunrise_time = time_new[i - 1]
        if irradiance_smooth(time_new)[i] <= 0.0 < irradiance_smooth(time_new)[i - 1]:
            sunset_time = time_new[i - 1]

    daylight_hours = sunset_time - sunrise_time
    print('\nDaylight time: ' + time_per_extent(daylight_hours))

    total_energy = np.trapz(irradiance_smooth(time_new) * 3600, time_new) * 10 ** (-6)  # units MJ/m**2
    print('Total energy received per area unit: %.2f MegaJoule / m**2' % total_energy)

    avg_irradiance = total_energy * 10 ** (6) / (daylight_hours * 3600)
    print('Average irradiance during daylight: %.2f W/m**2' % avg_irradiance)

    max_irradiance = max(irradiance_smooth(time_new))
    max_ir_time = time_new[np.where(irradiance_smooth(time_new) == max_irradiance)]
    print('Maximum irradiance: ' + str(round(max_irradiance, 2)) + ' W/m**2 at ' + time_per_extent(max_ir_time))

    return {'time': time_new, 'irradiance': irradiance_smooth(time_new), 'daylight_hours': daylight_hours,
            'total_energy': total_energy, 'avg_irradiance': avg_irradiance, 'max_ir_time': max_ir_time}


def plot_data(time, irradiance, year, month, day):
    # Daily irradiance distribution Plot
    fig = plt.figure(figsize=(10, 5))

    ax1 = fig.add_subplot(111)

    ax1.plot(time, irradiance, label='Date: ' + str(day) + '/' + str(month) + '/' + str(year))
    # plt.plot(data['time'], data['irradiance'], 'r*', markersize=4, )

    ax1.set_xlabel('Day Hour')
    ax1.set_ylabel('Irradiance, in W/m**2')

    ax1.legend(loc='best', prop={'size': 9})

    plt.xticks(data['time'])
    plt.grid()
    plt.show()


# Input
db_path = r'C:\solar_rad_model\SoDa_HC3-METEO_pampilhosa_da_serra.csv'
year = 2005
month_number = 12
day = 3
# --------------
data = read_df(db_path, year, month_number, day)
results = analyze_df(data)

if results != None:
    plot_data(results['time'], results['irradiance'],  year, month_number, day)
